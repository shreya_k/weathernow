var express = require('express');
var request = require('request');
var async = require('async');

var weatherNow = express();
weatherNow.get('/', function(req, res) {

	//Weather Underground API key
	var apiKey ='8c23059e8fce0a1c';
	
    //Locations for which weather data is required
    var locations = [ {'city':'Campbell','state':'CA','weatherData':'', 'image':''}, {'city':'Omaha','state':'NE','weatherData':'', 'image':''}, 
					  {'city':'Austin','state':'TX','weatherData':'', 'image':''}  , {'city':'Timonium','state':'MD','weatherData':'', 'image':''} ];
	
    //calling the function to get weather data, and then putting it together as an html page
	getWeatherData(locations, apiKey, function(error, weather_results) {
        if(!error) {
        	//creating a string with html tags and page title and heading
			var htmlString = '<!DOCTYPE html><html>\n\t<head>\n\t\t<title>WeatherNow</title>\n\t\t<link rel="shortcut icon" type="image/gif" href="//icons.wxug.com/i/c/c/partlycloudy.gif"/>\n\t</head>\n\t<body>\n\t\t<h1>WeatherNow - a weather app</h1>\n\t\t<h3>Weather Information: </h3>';
			
			//concatenating the weather results for each location to the string 
			for(var i =0; i<weather_results.length; i++) {
				htmlString += "\n\t\t<li>The weather in " + weather_results[i].city + ", " + weather_results[i].state + " is " + weather_results[i].weatherData + '</li>\n\t\t<img src="' + weather_results[i].image + '"><br><br>';
			}
			htmlString += "\n\t</body>\n</html>";
			res.send(htmlString);			
        }
    });
});

/**
 * Get Weather conditions for all the four locations asynchronously
 *
 * @param locations
 * @param apiKey
 * @param main_callback	the callback function to display the weather data
 */
function getWeatherData(locations, apiKey, main_callback) {
	//url format to get weather conditions. Details: http://www.wunderground.com/weather/api/d/docs?d=data/conditions
    var url_format = ('http://api.wunderground.com/api/apikey/conditions/q/state/city.json').replace('apikey',apiKey);
	
	//store url for each location
    var urls = [];
	//weather results for the required locations
    var weather_results = []; 

	//substitute location information in the url format string
    for(var i =0; i<locations.length; i++) {
      urls[i] = (url_format.replace('state',locations[i].state)).replace('city',locations[i].city);
    }

    //async.each applies the function to get data with each specific_url in the url array in parallel, 
    async.each (urls, function(specific_url, callback) {
                    
        	//makes the API call for the first location
            request(specific_url, function(error, response, json_string) { 
				//console.log(response);
			
				//console.log(json_string);
            	
            	//convert the json_string received to an object
                var weatherInfo=JSON.parse(json_string);
                
                //if an error is received, log it to console and callback
				if(weatherInfo.response.error) {
					console.log("Error", weatherInfo.response.error );
					callback(false);
					return;
				}
				
				//log to console the city and state for which information has been retrieved
                console.log('Weather data obtained for', weatherInfo.current_observation.display_location.city,',',
														 weatherInfo.current_observation.display_location.state);
				
					var city_weather = {};
					
					//get required information from json object and push it to the result array
					if(weatherInfo) {
						city_weather.city = weatherInfo.current_observation.display_location.city;
						city_weather.state = weatherInfo.current_observation.display_location.state;
						city_weather.weatherData = weatherInfo.current_observation.weather + ' with a temperature of ' +   
														weatherInfo.current_observation.temperature_string;
						city_weather.image = weatherInfo.current_observation.icon_url;
						
						weather_results.push(city_weather);
					}
				//callback as the task is done
                callback(false);
            });
    },
        
        //the callback for when each of the functions are finished
        function(error) {
            if(!error){
				console.log('Retrieved all the data, displaying it on localhost:8888 now');
			}
            //callback to display the results
            main_callback(false,weather_results);
        }
    );

}

//listen to port 8888
weatherNow.listen(8888);
console.log('Express started on port 8888');